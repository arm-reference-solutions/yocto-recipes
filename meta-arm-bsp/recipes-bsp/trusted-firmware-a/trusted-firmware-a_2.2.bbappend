# Machine specific TFAs

MACHINE_TFA_REQUIRE ?= ""
MACHINE_TFA_REQUIRE_sgi575 = "trusted-firmware-a-sgi575.inc"

require ${MACHINE_TFA_REQUIRE}
