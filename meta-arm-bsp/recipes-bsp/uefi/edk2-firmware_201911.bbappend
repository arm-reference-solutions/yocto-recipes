EDK2_SRC_URI = "git://git.linaro.org/landing-teams/working/arm/edk2.git"
EDK2_PLATFORMS_SRC_URI = "git://git.linaro.org/landing-teams/working/arm/edk2-platforms.git"

# TAGS and commit ID as per N1SDP-2020.03.26 release
SRCREV_edk2           = "3ff8a6bffabad761279817252014d59b6069d68a"
SRCREV_edk2-platforms = "d9d32e6c8b7456d21ce365486ac563b6be36c20d"

require edk2-firmware-n1sdp.inc
